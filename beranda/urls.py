from django.urls import path
from .views import *
from django.conf.urls.static import static
from django.conf import settings

app_name = 'beranda'
urlpatterns = [
    path('', BlogViews,name='post'),
    path('blog/<slug:slug>/', BlogDetail, name='detail'),
    path('kelas/', BlogDetail, name='test'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)