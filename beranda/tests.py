from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
import unittest


# Create your tests here.
class berandaTest(TestCase):
    '''test apakah beranda dapat diakses'''
    def test_beranda_url_is_exist(self):
        response = Client().get('/ask')
        self.assertEqual(response.status_code,301)

    '''test apakah menggunakan template bernama LandingPage'''
    def test_beranda_using_to_do_list_template(self):
        response = Client().get('/ask/')
        self.assertTemplateUsed(response, 'index_2.html')

    '''test menggunakan fungsi index yang ada didalam views'''
    def test_beranda_using_index_func(self):
        found = resolve('/ask/')
        self.assertEqual(found.func, BlogViews)
    
    def test_beranda_using_index_func(self):
        found = resolve('/ask/kelas/')
        self.assertEqual(found.func, BlogDetail)


    def test_empty_string_in_models(self):
        form = CommentForm(data={'Nama': ''})
        self.assertFalse(form.is_valid())
        
