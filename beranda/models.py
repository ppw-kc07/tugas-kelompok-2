from django.db import models
# Create your models here.

class Blog(models.Model):
    Title = models.CharField(max_length = 300)
    Content = models.CharField(max_length=1000)
    Slug = models.SlugField(max_length=100,unique=True, default ='')
    Date = models.DateTimeField(auto_now_add = True)

    class Meta:
        ordering = ('-Date',)

    def __str__(self):
        return self.Title
    
    def __unicode__(self):
        return u'%s' % self.Title

class Comment(models.Model):
    Post = models.ForeignKey(Blog, on_delete=models.CASCADE, related_name='comments')
    Name = models.CharField(max_length=100, default="Unknown")
    Body = models.CharField(max_length=500)
    Created = models.DateTimeField(auto_now_add=True)
    Active = models.BooleanField(default=True)
    Parent = models.ForeignKey('self', on_delete=models.CASCADE, null = True, blank=True, related_name='replies')

    class Meta:
       ordering = ('-Created',)

    def __str__(self):
        return 'Comment By {}'.format(self.Name)


