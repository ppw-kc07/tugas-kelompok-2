from django import forms
from .models import *

class CommentForm(forms.ModelForm):
    Body = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'input',
            'placeholder' : 'Tulis disini...'
        }
    ))
    Name = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'input',
            'placeholder' : 'Tulis nama kamu disini...'
        }
    ))
    class Meta:
        model = Comment
        fields = ('Name', 'Body')
        widgets = {
            'Name' : forms.TextInput(attrs={'class':'input'}),
        }