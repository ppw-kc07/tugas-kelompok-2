from django.shortcuts import render, redirect, HttpResponse, get_object_or_404, HttpResponseRedirect
from .models import *
from .forms import *

# Create your views here.
def BlogViews(request):
    posts = Blog.objects.all()
    return render(request, 'index_2.html', {'post':posts})

def BlogDetail(request, slug):
    post = get_object_or_404(Blog, Slug = slug)
    comments = post.comments.filter(Active=True, Parent__isnull = True)
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid:
            Parent_obj = None
            try :
                Parent_id = int(request.POST.get('Parent_id'))
            except :
                Parent_id = None
            if Parent_id:
                Parent_obj = Comment.objects.get(id=Parent_id)
                if Parent_obj:
                    reply_comment = comment_form.save(commit=False)
                    reply_comment.Parent = Parent_obj
            new_comment = comment_form.save(commit=False)
            new_comment.Post = post
            new_comment.save()
            return redirect('beranda:detail', slug)
    else:
        comment_form = CommentForm()

    return render(request, 'blogdetail.html', {'blog':post, 'comments':comments, 'comment_form':comment_form})