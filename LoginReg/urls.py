from django.urls import path
from .views import index, login, registration, logout

app_name = 'LoginReg'

urlpatterns = [
    path('', index, name="index"),
    path('login/', login, name="login"),
    path('logout/', logout, name="logout"),
    path('registration/', registration, name="registration")
]