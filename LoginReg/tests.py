from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.contrib.auth.models import User
from .views import index, login, registration


# Create your tests here.
class UnitTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user('testaja', 'cobadulu')
        cls.user.save()

    def test_login_is_exist(self):
        response = Client().get('/acc/login/')
        self.assertEqual(response.status_code, 200)


    def test_registration_is_exist(self):
        response = Client().get('/acc/registration/')
        self.assertEqual(response.status_code, 200)

    def test_check_login_contains(self):
        response = self.client.get('/acc/login/')
        self.assertContains(response, 'username')
        self.assertContains(response, 'password')

    def test_login(self):
        response = self.client.post('/acc/login/', data={
                'username': 'testaja',
                'password' : 'cobadulu',
            }
        )
        response = self.client.get('/acc/')
        self.assertEqual(response.status_code, 200)

    def test_login_empty(self):
        response = self.client.post('/acc/login/', data={
                'username': '',
                'password' : '',
            }
        )
        response = self.client.get('/acc/login/')
        self.assertEqual(response.status_code, 200)

    def test_check_registration_contains(self):
        response = self.client.get('/acc/registration/')
        self.assertContains(response, 'username')
        self.assertContains(response, 'password')
        self.assertContains(response, 'password2')

    def test_registration(self):
        response = self.client.post('/acc/registration/', data={
                'username': 'testaja1',
                'password' : 'cobadulu',
                'password2' : 'cobadulu',
            }
        )
        response = self.client.get('/acc/registration/')
        self.assertEqual(response.status_code, 200)

    def test_registration_exist(self):
        response = self.client.post('/acc/registration/', data={
                'username': 'testaja',
                'password' : 'cobadulu',
                'password2' : 'cobadulu',
            }
        )
        response = self.client.get('/acc/registration/')
        self.assertEqual(response.status_code, 200)

    def test_registration_password_only_7(self):
        response = self.client.post('/acc/registration/', data={
                'username': 'testaja1',
                'password' : 'cobadul',
                'password2' : 'cobadul',
            }
        )
        response = self.client.get('/acc/registration/')
        self.assertEqual(response.status_code, 200)

    def test_registration_diff_pass(self):
        response = self.client.post('/acc/registration/', data={
                'username': 'testaja',
                'password' : 'cobadulu',
                'password2' : 'cobadulu1',
            }
        )
        response = self.client.get('/acc/registration/')
        self.assertEqual(response.status_code, 200)

    def test_registration_empty(self):
        response = self.client.post('/acc/registration/', data={
                'username': '',
                'password' : '',
                'password2' : '',
            }
        )
        response = self.client.get('/acc/registration/')
        self.assertEqual(response.status_code, 200)

    def test_logout(self):
        response = self.client.post('/acc/login/', data={
                'username': 'testaja',
                'password' : 'cobadulu',
            }
        )
        self.client.get('/acc/logout/')
        response = self.client.get('/acc/')
        html = response.content.decode()
        self.assertNotIn(self.user.username, html)

    def test_using_index_func(self):
        found = resolve('/acc/')
        self.assertEqual(found.func, index)
